FROM openjdk:17-jdk-alpine3.14 as build

WORKDIR /app

COPY . .

RUN apk --no-cache add curl && \
    curl -L https://services.gradle.org/distributions/gradle-8.5-bin.zip -o gradle.zip && \
    unzip gradle.zip -d /opt && \
    rm gradle.zip

ENV GRADLE_HOME=/opt/gradle-8.5
ENV PATH=$PATH:$GRADLE_HOME/bin

RUN gradle --version

RUN gradle build -x test

EXPOSE 8080

FROM openjdk:17-jdk-alpine3.14 as production
WORKDIR /app
COPY --from=build /app/build/libs/project-0.0.1.jar .

ENTRYPOINT ["java","-jar","project-0.0.1.jar"]