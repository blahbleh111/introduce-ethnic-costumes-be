package com.group.project.service;

import com.group.project.dto.ethnics.EthnicsDto;
import com.group.project.entity.EthnicsEntity;
import com.group.project.repository.EthnicsRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.assertj.core.api.Assertions;

import java.time.LocalDateTime;

import java.util.List;


@SpringBootTest
@AutoConfigureMockMvc
public class EthnicsServiceTest {
    @Autowired
    EthnicsService ethnicsService;

    @MockBean
    EthnicsRepository ethnicsRepository;

    @BeforeEach
    public void setUp() {

    }

    @AfterEach
    public void tearDown() {

    }

    @Test()
    public void getAllEthnicsTest() {
        EthnicsEntity ethnicsEntity1 = new EthnicsEntity();

        ethnicsEntity1.setId(1L);
        ethnicsEntity1.setName("Ethnics");
        ethnicsEntity1.setDescription("Ethnics");
        ethnicsEntity1.setOrigin("Origin");
        ethnicsEntity1.setImageUrl("URL");
        ethnicsEntity1.setCreatedAt(LocalDateTime.now());
        ethnicsEntity1.setUpdatedAt(LocalDateTime.now());

        Mockito.when(ethnicsRepository.findAll()).thenReturn(List.of(ethnicsEntity1));

        List <EthnicsDto> ethnicsDtoList= ethnicsService.getAllEthnics();
        Assertions.assertThat(ethnicsDtoList).hasSize(1);
        Assertions.assertThat(ethnicsDtoList.get(0).getId()).isEqualTo(1);
    }

}
