package com.group.project.model;

import com.group.project.entity.EthnicsEntity;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Costume {
    private Long id;

    private Long ethnicId;

    private String material;

    private String pattern;

    private String detail;

    private String characteristic;

    private String imageUrl;

    private Float price;

    private String currency;

    private String other;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private List<CostumesDetail> listCostumesDetail;
}
