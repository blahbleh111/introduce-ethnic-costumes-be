package com.group.project.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class InventoryCostume {
    private Long id;
    private Long costumeId;
    private String size;
    private Long quantity;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
