package com.group.project.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Ethnics {
    private Long id;

    private String name;
    private String description;
    private String origin;
    private String imageUrl;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
