package com.group.project.model;

import com.group.project.entity.CostumeEntity;
import com.group.project.entity.OrderEntity;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class OrderDetail {
    private Long id;
    private Long orderId;
    private Long costumeId;
    private Long quantity;
    private Float price;
    private String size;
    private String name;
    private String phoneNumber;
    private String address;
}
