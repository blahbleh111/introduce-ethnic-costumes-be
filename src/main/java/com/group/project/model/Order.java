package com.group.project.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Order {
    private Long id;
    private Long userId;
    private Float totalAmount;
    private LocalDateTime orderDate;
    private String status;
}
