package com.group.project.model;

import com.group.project.entity.CostumeEntity;
import com.group.project.entity.UserEntity;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Comment {
    private Long id;

    private String content;

    private Long userId;

    private Long costumeId;

    private String status;

    private LocalDateTime createdAt;
}
