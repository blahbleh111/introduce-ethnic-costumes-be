package com.group.project.model;

import lombok.Data;

@Data
public class CostumesDetail {
    private Long id;

    private Long costumeId;

    private String sex;

    private String description;
}
