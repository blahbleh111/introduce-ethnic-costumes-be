package com.group.project.service;

import com.group.project.dto.costumes.CostumeDto;
import com.group.project.dto.costumes.CostumesDetailDto;
import com.group.project.entity.CostumeEntity;
import com.group.project.entity.CostumesDetailEntity;
import com.group.project.model.Costume;
import com.group.project.model.CostumesDetail;
import com.group.project.repository.CostumeRepository;
import com.group.project.repository.CostumesDetailRepository;
import com.group.project.utils.mapper.Mapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CostumeService {
    private final Mapper mapper;
    private final CostumeRepository costumeRepository;
    private final CostumesDetailRepository costumesDetailRepository;

    public CostumeService(CostumeRepository costumeRepository, CostumesDetailRepository costumesDetailRepository, Mapper mapper) {
        this.costumeRepository = costumeRepository;
        this.costumesDetailRepository = costumesDetailRepository;
        this.mapper = mapper;
    }

    public List<CostumeDto> findAllCostumes() {
        List<Costume> costumeEntityList = costumeRepository.findAllWithCostumesId().stream().map(costumeEntity -> mapper.mapEntityToModel(costumeEntity,Costume.class)).collect(Collectors.toList());

        return costumeEntityList.stream().map(costume -> mapper.mapModelToDto(costume,CostumeDto.class)).collect(Collectors.toList());
    }

    public CostumeDto findCostumeByEthnicId(Long ethnicId) {
        Costume costume = mapper.mapEntityToModel(costumeRepository.findByEthnicIdWithCostumesId(ethnicId),Costume.class);

        return mapper.mapModelToDto(costume, CostumeDto.class);
    }

    public String editCostumeInfoById(CostumeDto costumeEdited) {

        Costume costume = mapper.mapEntityToModel(costumeRepository.findById(costumeEdited.getId()), Costume.class);
        if (costumeEdited.getCharacteristic() != null && !Objects.equals(costumeEdited.getCharacteristic(), "")) {
            costume.setCharacteristic(costumeEdited.getCharacteristic());
        }
        if (costumeEdited.getPattern() != null && !Objects.equals(costumeEdited.getPattern(), "")) {
            costume.setPattern(costumeEdited.getPattern());
        }
        if (costumeEdited.getMaterial() != null && !Objects.equals(costumeEdited.getMaterial(), "")) {
            costume.setMaterial(costumeEdited.getMaterial());
        }
        if (costumeEdited.getOther() != null && !Objects.equals(costumeEdited.getOther(), "")) {
            costume.setOther(costumeEdited.getOther());
        }
        costume.setUpdatedAt(LocalDateTime.now());

        costumeRepository.save(mapper.mapModelToEntity(costume, CostumeEntity.class));

        return "OK";
    }

    public String editCostumesDetailById(CostumesDetailDto costumesDetailEdited) {
        CostumesDetail costumesDetail = mapper.mapEntityToModel(costumesDetailRepository.findById(costumesDetailEdited.getSexId()), CostumesDetail.class);

        costumesDetail.setDescription(costumesDetailEdited.getDescription());

        costumesDetailRepository.save(mapper.mapModelToEntity(costumesDetail, CostumesDetailEntity.class));

        return "OK";
    }

}
