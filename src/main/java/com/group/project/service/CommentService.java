package com.group.project.service;

import com.group.project.dto.comment.CommentDto;
import com.group.project.dto.comment.GetCommentResponseDto;
import com.group.project.entity.CommentEntity;
import com.group.project.exception.AppException;
import com.group.project.model.Comment;
import com.group.project.repository.CommentRepository;
import com.group.project.utils.mapper.Mapper;
import com.group.project.utils.security.UserDetailsImpl;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommentService {
    private final Mapper mapper;
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository, Mapper mapper) {
        this.commentRepository = commentRepository;
        this.mapper = mapper;
    }

    public Long getUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return userDetails.getId();
    }

    public List<CommentDto> getCommentsByCostumesId(Long costumesId) {
        List<Comment> commentList = commentRepository.findByCostumeId(costumesId).stream().map(commentEntity -> mapper.mapEntityToModel(commentEntity, Comment.class)).collect(Collectors.toList());
        return commentList.stream().map(comment -> mapper.mapModelToDto(comment, CommentDto.class)).collect(Collectors.toList());
    }

    public List<GetCommentResponseDto> getAllCommentsByCostumesId(Long costumesId) {
        List<Object[]> commentsList = commentRepository.findCommentAndUserInfoByCostumeId(costumesId);

        return commentsList.stream().map(comment -> mapper.mapToDto(comment)).toList();


    }

    @Transactional
    public CommentDto addComment(String content, Long costumeId) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setUserId(getUserId());
        comment.setCostumeId(costumeId);
        comment.setStatus("0"); // TODO  ADD ENUM FOR THIS
        comment.setCreatedAt(LocalDateTime.now());
        CommentEntity commentEntity = commentRepository.save(mapper.mapModelToEntity(comment, CommentEntity.class));
        comment = mapper.mapEntityToModel(commentEntity, Comment.class);
        return mapper.mapModelToDto(comment, CommentDto.class);
    }

    @Transactional
    public String updateComment(String content, Long costumeId, Long id) {
        Comment comment = mapper.mapEntityToModel(commentRepository.findByIdAndUserIdAndCostumeId(id, getUserId(), costumeId), Comment.class);
        if (Objects.equals(comment.getStatus(), "1")) {
            throw new EntityNotFoundException("Comment not found");
        }
        comment.setContent(content);

        commentRepository.save(mapper.mapModelToEntity(comment, CommentEntity.class));
        return "Updated Comment Success"; //TODO add responseDto

    }

    @Transactional
    public String deleteComment(Long id, Long costumeId, Long userId) {
        Comment comment = mapper.mapEntityToModel(commentRepository.findByIdAndCostumeId(id, costumeId), Comment.class);

        if (comment == null || Objects.equals(comment.getStatus(), "1")) {
            throw new EntityNotFoundException("Comment not found");
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (isAdminOrCurrentUser(userDetails, userId)) {
            comment.setStatus("1");
            commentRepository.save(mapper.mapModelToEntity(comment, CommentEntity.class));
            return "Delete success";
        } else {
            throw new BadCredentialsException("You are not allowed");
        }

    }

    private boolean isAdminOrCurrentUser(UserDetails userDetails, Long userId) {
        for (GrantedAuthority authority : userDetails.getAuthorities()) {
            if ("admin".equals(authority.getAuthority())) {
                return true;
            }
        }
        return Objects.equals(getUserId(), userId);
    }
}
