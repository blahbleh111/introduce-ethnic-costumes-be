package com.group.project.service;

import com.group.project.dto.order.OrderDetailDto;
import com.group.project.dto.order.OrderDto;
import com.group.project.entity.OrderDetailEntity;
import com.group.project.entity.OrderEntity;
import com.group.project.model.Order;
import com.group.project.model.OrderDetail;
import com.group.project.repository.OrderDetailRepository;
import com.group.project.repository.OrderRepository;
import com.group.project.utils.mapper.Mapper;
import com.group.project.utils.security.UserDetailsImpl;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
public class OrderService {
    private final Mapper mapper;

    private final OrderRepository orderRepository;

    private final OrderDetailRepository orderDetailRepository;

    private final InventoryCostumeService inventoryCostumeService;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    private final String TOPIC = "order_topic";

    public OrderService(Mapper mapper, OrderRepository orderRepository, InventoryCostumeService inventoryCostumeService, OrderDetailRepository orderDetailRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.mapper = mapper;
        this.orderRepository = orderRepository;
        this.inventoryCostumeService = inventoryCostumeService;
        this.orderDetailRepository = orderDetailRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public List<OrderDto> getOrderOfCurrentUser() {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Order> orderList = orderRepository.findAllByUserId(userDetails.getId()).stream().map(orderEntity -> mapper.mapEntityToModel(orderEntity, Order.class)).toList();

        return orderList.stream().map(order -> mapper.mapModelToDto(order, OrderDto.class)).toList();
    }

    public List<OrderDetailDto> getOrdersByOrderId(Long orderId) {
        List<OrderDetail> orderDetailsList = orderDetailRepository.findByOrderId(orderId).stream().map(orderEntity -> mapper.mapEntityToModel(orderEntity, OrderDetail.class)).toList();
        return orderDetailsList.stream().map(order -> mapper.mapModelToDto(order, OrderDetailDto.class)).toList();
    }

    public List<OrderDto> getAllOrder() {
        List<Order> orderList = orderRepository.findAll().stream().map(orderEntity -> mapper.mapEntityToModel(orderEntity, Order.class)).toList();

        return orderList.stream().map(order -> mapper.mapModelToDto(order, OrderDto.class)).toList();
    }
    @Transactional
    public String createOrder(Long costumeId, Long quantity, Float price, String size, String name, String phoneNumber, String address) {
        Order order = new Order();
        order.setStatus("Tiền Mặt");
        order.setOrderDate(LocalDateTime.now());
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        order.setUserId(userDetails.getId());
        float totalAmount = 0;
//        for (OrderDetail detail : orderDetails) {
            totalAmount += price * quantity;
//        }
        order.setTotalAmount(totalAmount);
        OrderEntity savedOrderEntity = orderRepository.save(mapper.mapModelToEntity(order, OrderEntity.class));

        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setOrderId(savedOrderEntity.getId());
        orderDetail.setQuantity(quantity);
        orderDetail.setSize(size);
        orderDetail.setPrice(price);
        orderDetail.setCostumeId(costumeId);
        orderDetail.setName(name);
        orderDetail.setPhoneNumber(phoneNumber);
        orderDetail.setAddress(address);

        orderDetailRepository.save(mapper.mapModelToEntity(orderDetail, OrderDetailEntity.class));

//        for (OrderDetail detail : orderDetails) {
            inventoryCostumeService.removeCostumeFromInventory(costumeId, size, quantity);
//        }
        kafkaTemplate.send(TOPIC, mapper.mapEntityToModel(savedOrderEntity, Order.class));
        return "Create Order Successfully";
    }

    public OrderDetail getOrderDetailById(Long orderId) {
        return mapper.mapModelToEntity(orderDetailRepository.findById(orderId).orElse(null), OrderDetail.class);
    }

    @Transactional
    public String cancelOrder(Long orderId) {
        OrderEntity orderEntity = orderRepository.findByOrderIdWhenDelete(orderId);
        if (orderEntity == null ) {
            throw new EntityNotFoundException("Order Not Found");
        }
        OrderDetail orderDetail = mapper.mapEntityToModel(orderDetailRepository.findByOrderIdWhenDelete(orderId), OrderDetail.class);
        if (orderDetail ==null) {
            throw new EntityNotFoundException("OrderDetail Not Found");
        }
        inventoryCostumeService.addCostumeToInventory(orderDetail.getCostumeId(), orderDetail.getSize(), orderDetail.getQuantity());

        orderDetailRepository.delete(mapper.mapModelToEntity(orderDetail, OrderDetailEntity.class));
        orderRepository.delete(orderEntity);

        return "Cancel Order Successfully";
    }

}
