package com.group.project.service;

import com.group.project.dto.inventory.InventoryCostumeDto;
import com.group.project.entity.InventoryCostumeEntity;
import com.group.project.exception.AppException;
import com.group.project.model.InventoryCostume;
import com.group.project.repository.InventoryCostumeRepository;
import com.group.project.utils.mapper.Mapper;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class InventoryCostumeService {
    private final Mapper mapper;
    private final InventoryCostumeRepository inventoryCostumeRepository;

    public InventoryCostumeService(Mapper mapper, InventoryCostumeRepository inventoryCostumeRepository) {
        this.mapper = mapper;
        this.inventoryCostumeRepository = inventoryCostumeRepository;
    }

    public List<InventoryCostumeDto> getAll() {
        List<InventoryCostume> inventoryCostumesList =
                inventoryCostumeRepository.findAll().stream().map(inventoryCostumeEntity -> mapper.mapEntityToModel(inventoryCostumeEntity, InventoryCostume.class)).toList();

        return inventoryCostumesList.stream().map(inventoryCostume -> mapper.mapModelToDto(inventoryCostume, InventoryCostumeDto.class)).toList();
    }

    public String addCostumeToInventory(Long costumeId, String size, Long quantity) {
        InventoryCostumeEntity inventoryItem = inventoryCostumeRepository.findByCostumeIdAndSize(costumeId, size);
        if (inventoryItem != null) {
            inventoryItem.setQuantity(inventoryItem.getQuantity() + quantity);
            inventoryItem.setUpdatedAt(LocalDateTime.now());
        } else {
            inventoryItem = new InventoryCostumeEntity();
            inventoryItem.setCostumeId(costumeId);
            inventoryItem.setSize(size);
            inventoryItem.setQuantity(quantity);
            inventoryItem.setCreatedAt(LocalDateTime.now());
        }
        inventoryCostumeRepository.save(inventoryItem);
        return "Add inventory costume successfully";
    }

    public String removeCostumeFromInventory(Long costumeId, String size, Long quantity) {
        InventoryCostumeEntity inventoryItem = inventoryCostumeRepository.findByCostumeIdAndSize(costumeId, size);
        if (inventoryItem != null) {
            long newQuantity = inventoryItem.getQuantity() - quantity;
            if (newQuantity < 0) {
                throw new IllegalStateException("Cannot remove inventory costume. Quantity would be negative");
            } else {
                inventoryItem.setQuantity(newQuantity);
                inventoryItem.setUpdatedAt(LocalDateTime.now());
                inventoryCostumeRepository.save(inventoryItem);
            }
        } else {
            throw new EntityNotFoundException("Inventory costume not found.");
        }
        return "Remove inventory costume successfully";
    }

    @Transactional
    public String deleteCostumeFromInventory(Long id) throws AppException {
        Optional<InventoryCostumeEntity> inventoryItemOptional = inventoryCostumeRepository.findById(id);
        if (inventoryItemOptional.isEmpty()) {
            throw new AppException("Inventory item not found");
        }

        InventoryCostumeEntity inventoryItem = inventoryItemOptional.get();
        inventoryCostumeRepository.delete(inventoryItem);

        return "Deleted inventory costume successfully";
    }

    public Long getStockQuantity(Long costumeId, String size) {
        InventoryCostumeEntity inventoryItem = inventoryCostumeRepository.findByCostumeIdAndSize(costumeId, size);
        return (inventoryItem != null) ? inventoryItem.getQuantity() : 0;
    }

}
