package com.group.project.service;

import com.group.project.dto.auth.SignInRequestDto;
import com.group.project.entity.UserEntity;
import com.group.project.exception.EmailAlreadyTakenException;
import com.group.project.exception.UsernameAlreadyTakenException;
import com.group.project.model.User;
import com.group.project.repository.UserRepository;
import com.group.project.utils.mapper.Mapper;
import com.group.project.utils.security.JwtUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private final Mapper mapper;
    private final UserRepository userRepository;

    private final PasswordEncoder encoder;

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    public UserService(Mapper mapper, UserRepository userRepository, PasswordEncoder encoder, AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.userRepository = userRepository;
        this.mapper = mapper;
        this.encoder = encoder;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    public String signup(String username, String email ,String password) throws UsernameAlreadyTakenException, EmailAlreadyTakenException {
        if (userRepository.existsByUsername(username)) {
            throw new UsernameAlreadyTakenException("Error: Username is already taken!");
        }
        if (userRepository.existsByEmail(email)) {
            throw new EmailAlreadyTakenException("Error: Email is already taken!");
        }
        int random = ((int) (Math.random() * 100) + 1);
        User user = new User(username,email, encoder.encode(password),"user");
        user.setUrl("https://avatar.iran.liara.run/public/" + random);

        userRepository.save(mapper.mapModelToEntity(user, UserEntity.class));

        return "Success!";
    }

    public String login(SignInRequestDto reqDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(reqDto.getUsername(), reqDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        return jwtUtils.generateJwtToken(authentication);
    }

    public String changePass(Long id , String newPassword) {
        User user = mapper.mapEntityToModel(userRepository.findById(id), User.class);
        user.setPassword(encoder.encode(newPassword));
        userRepository.save(mapper.mapModelToEntity(user,UserEntity.class)); ;

        return "ok";
    }

}
