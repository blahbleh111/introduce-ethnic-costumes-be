package com.group.project.service;

import com.group.project.dto.ethnics.EthnicsDto;
import com.group.project.model.Ethnics;
import com.group.project.repository.EthnicsRepository;
import com.group.project.utils.mapper.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EthnicsService {
    private final Mapper mapper;

    private final EthnicsRepository ethnicsRepository;

    public EthnicsService(Mapper mapper, EthnicsRepository ethnicsRepository) {
        this.mapper = mapper;
        this.ethnicsRepository = ethnicsRepository;
    }

    public List<EthnicsDto> getAllEthnics() {
        List<Ethnics> ethnicsList = ethnicsRepository.findAll().stream().map(ethnicsEntity -> mapper.mapEntityToModel(ethnicsEntity, Ethnics.class)).collect(Collectors.toList());
        return ethnicsList.stream().map(ethnic -> mapper.mapModelToDto(ethnic, EthnicsDto.class)).collect(Collectors.toList());
    }

    public EthnicsDto getEthnicById(Long id) {
        Ethnics ethnics = mapper.mapEntityToModel(ethnicsRepository.findById(id),Ethnics.class);
        return mapper.mapModelToDto(ethnics, EthnicsDto.class);
    }

    public List<EthnicsDto> searchEthnics(String name) {
        List<Ethnics> ethnicsList = ethnicsRepository.findByNameContains(name).stream().map(ethnicsEntity -> mapper.mapEntityToModel(ethnicsEntity,Ethnics.class)).collect(Collectors.toList());
        return ethnicsList.stream().map(ethnics -> mapper.mapModelToDto(ethnics,EthnicsDto.class)).collect(Collectors.toList());
    }

}
