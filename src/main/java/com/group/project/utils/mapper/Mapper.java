package com.group.project.utils.mapper;

import com.group.project.dto.comment.CommentDto;
import com.group.project.dto.comment.GetCommentResponseDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class Mapper {
    private final ModelMapper mapper;

    public Mapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public <S,T> T mapEntityToModel(S entity, Class<T> model) {
        return mapper.map(entity, model);
    }

    public <S,T> T mapModelToDto(S model, Class<T> dto) { return mapper.map(model, dto); }

    public <S,T> T mapModelToEntity(S model, Class<T> entity) { return mapper.map(model, entity); }

    public GetCommentResponseDto mapToDto(Object[] object) {
        CommentDto commentDto = this.mapModelToDto(object[0], CommentDto.class);

        String name = (String) object[1];

        String url = (String) object[2];

        GetCommentResponseDto responseDto = new GetCommentResponseDto();
        responseDto.setCommentDto(commentDto);
        responseDto.setName(name);
        responseDto.setUrl(url);

        return responseDto;
    }

}
