package com.group.project.repository;

import com.group.project.entity.CostumeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CostumeRepository extends JpaRepository<CostumeEntity, Long> {
    @Query("SELECT c from CostumeEntity c JOIN FETCH c.listCostumesDetail ")
    List<CostumeEntity> findAllWithCostumesId();

    @Query("select c FROM CostumeEntity c JOIN FETCH c.listCostumesDetail where c.ethnicId = :ethnicId")
    CostumeEntity findByEthnicIdWithCostumesId(Long ethnicId);
}
