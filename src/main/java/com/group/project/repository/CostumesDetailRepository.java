package com.group.project.repository;

import com.group.project.entity.CostumesDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CostumesDetailRepository extends JpaRepository<CostumesDetailEntity,Long> {
}
