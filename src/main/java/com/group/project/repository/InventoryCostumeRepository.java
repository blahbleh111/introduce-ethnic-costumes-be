package com.group.project.repository;

import com.group.project.entity.InventoryCostumeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryCostumeRepository extends JpaRepository<InventoryCostumeEntity,Long> {
    InventoryCostumeEntity findByCostumeIdAndSize(Long costumeId, String size);
}
