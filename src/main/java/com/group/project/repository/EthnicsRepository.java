package com.group.project.repository;

import com.group.project.entity.EthnicsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EthnicsRepository extends JpaRepository<EthnicsEntity, Long> {
    List<EthnicsEntity> findByNameContains(String name);
}
