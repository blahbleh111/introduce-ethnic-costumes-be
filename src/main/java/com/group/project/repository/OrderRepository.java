package com.group.project.repository;

import com.group.project.entity.OrderEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {

    List<OrderEntity> findAllByUserId(Long userId);

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query("SELECT o from OrderEntity o WHERE o.id = :orderId")
    OrderEntity findByOrderIdWhenDelete(Long orderId);
}
