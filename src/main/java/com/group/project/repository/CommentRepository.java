package com.group.project.repository;

import com.group.project.entity.CommentEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {

    @Query("select ce, u.username, u.url from CommentEntity ce join fetch UserEntity u on ce.userId = u.id where ce.costumeId = :costumeId")
    List<Object[]> findCommentAndUserInfoByCostumeId(Long costumeId);
//    @Lock(LockModeType.PESSIMISTIC_READ)
    CommentEntity findByIdAndUserIdAndCostumeId(Long id, Long userId, Long costumeId);

    List<CommentEntity> findByCostumeId(Long costumeId);

    @Lock(LockModeType.PESSIMISTIC_READ)
    CommentEntity findByIdAndCostumeId(Long id, Long costumeId);
}
