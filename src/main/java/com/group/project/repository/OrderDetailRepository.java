package com.group.project.repository;

import com.group.project.entity.OrderDetailEntity;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetailEntity, Long> {

    List<OrderDetailEntity> findByOrderId(Long orderId);

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query("SELECT od FROM OrderDetailEntity od WHERE od.orderId = :orderId")
    OrderDetailEntity findByOrderIdWhenDelete(Long orderId);

}
