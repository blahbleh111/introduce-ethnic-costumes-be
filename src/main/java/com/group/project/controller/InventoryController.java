package com.group.project.controller;

import com.group.project.dto.inventory.AddInventoryCostumeRequestDto;
import com.group.project.dto.inventory.DeleteInventoryCostumeRequestDto;
import com.group.project.dto.inventory.RemoveInventoryCostumeRequestDto;
import com.group.project.exception.AppException;
import com.group.project.service.InventoryCostumeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InventoryController {
    private final InventoryCostumeService inventoryCostumeService;

    public InventoryController(InventoryCostumeService inventoryCostumeService) {
        this.inventoryCostumeService = inventoryCostumeService;
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/add-inventory")
    public ResponseEntity<?> addInventoryCostume(@RequestBody AddInventoryCostumeRequestDto reqDto) {
        return ResponseEntity.ok(inventoryCostumeService.addCostumeToInventory(reqDto.getCostumeId(),reqDto.getSize(), reqDto.getQuantity()));
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/remove-inventory")
    public ResponseEntity<?> removeInventoryCostume(@RequestBody RemoveInventoryCostumeRequestDto reqDto) {
        return ResponseEntity.ok(inventoryCostumeService.removeCostumeFromInventory(reqDto.getCostumeId(),reqDto.getSize(),reqDto.getQuantity()));
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/get-all-inventories")
    public ResponseEntity<?> getAllInventoryCostumes() {
        return ResponseEntity.ok(inventoryCostumeService.getAll());
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/delete-inventory")
    public ResponseEntity<?> deleteInventoryCostume(@RequestBody DeleteInventoryCostumeRequestDto reqDto) throws AppException {
        return ResponseEntity.ok(inventoryCostumeService.deleteCostumeFromInventory(reqDto.getId()));
    }
}
