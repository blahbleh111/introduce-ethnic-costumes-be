package com.group.project.controller;

import com.group.project.dto.order.CreateOrderRequestDto;
import com.group.project.service.OrderService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderController {
    public final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PreAuthorize("hasAnyAuthority( 'user')")
    @PostMapping("/create-order")
    public ResponseEntity<?> createOrder(@Valid @RequestBody CreateOrderRequestDto reqDto) {
        return ResponseEntity.ok(orderService.createOrder(reqDto.getCostumeId(), reqDto.getQuantity(), reqDto.getPrice(), reqDto.getSize(), reqDto.getName(), reqDto.getPhoneNumber(), reqDto.getAddress()));
    }

    @PreAuthorize("hasAnyAuthority( 'user', 'admin')")
    @GetMapping("/get-order")
    public ResponseEntity<?> getOrderOfCurrentUser() {
        return ResponseEntity.ok(orderService.getOrderOfCurrentUser());
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @GetMapping("/get-all-orders")
    public ResponseEntity<?> getAllOrders() {
        return ResponseEntity.ok(orderService.getAllOrder());
    }

    @PreAuthorize("hasAnyAuthority( 'user', 'admin')")
    @GetMapping("/get-order/{id}")
    public ResponseEntity<?> getOrderById(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.getOrdersByOrderId(id));
    }

    @PreAuthorize("hasAnyAuthority('user', 'admin')")
    @PostMapping("/cancel-order/{id}")
    public ResponseEntity<?> cancelOrder(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.cancelOrder(id));
    }

}
