package com.group.project.controller;

import com.group.project.dto.ethnics.EthnicsDto;
import com.group.project.dto.ethnics.SearchEthnicsRequestDto;
import com.group.project.service.EthnicsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EthnicsController {
    private final EthnicsService ethnicsService;

    public EthnicsController(EthnicsService ethnicsService) {
        this.ethnicsService = ethnicsService;
    }

    @GetMapping("/get-ethnics")
    public List<EthnicsDto> getAllEthnics() {
        return ethnicsService.getAllEthnics();
    }

    @PostMapping("/search-ethnics")
    public List<EthnicsDto> searchEthnics(@RequestBody SearchEthnicsRequestDto reqDto) {
        return ethnicsService.searchEthnics(reqDto.getName());
    }
    @GetMapping("/get-ethnics/{id}")
    public EthnicsDto getEthnics(@PathVariable Long id) {
        return ethnicsService.getEthnicById(id);
    }

}
