package com.group.project.controller;

import com.group.project.dto.comment.AddCommentRequestDto;
import com.group.project.dto.comment.DeleteCommentRequestDto;
import com.group.project.dto.comment.GetCommentRequestDto;
import com.group.project.dto.comment.UpdateCommentRequestDto;
import com.group.project.exception.AppException;
import com.group.project.service.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping(value = "/get-comments")
    public ResponseEntity<?> getAllComments(@RequestBody GetCommentRequestDto reqDto) {
        return ResponseEntity.ok(commentService.getCommentsByCostumesId(reqDto.getCostumeId()));
    }

    @PreAuthorize("hasAnyAuthority('user')")
    @PostMapping(value = "/post-comments")
    public ResponseEntity<?> addComment(@RequestBody AddCommentRequestDto reqDto) {
        return ResponseEntity.ok(commentService.addComment(reqDto.getContent(),reqDto.getCostumeId()));
    }

    @PreAuthorize("hasAnyAuthority('user')")
    @PostMapping(value = "/update-comments")
    public ResponseEntity<?> updateComment(@RequestBody UpdateCommentRequestDto reqDto) {
        return ResponseEntity.ok(commentService.updateComment(reqDto.getContent(),reqDto.getCostumeId(), reqDto.getId()));
    }

    @PreAuthorize("hasAnyAuthority('admin', 'user')")
    @PostMapping(value = "/delete-comments")
    public ResponseEntity<?> deleteComment(@RequestBody DeleteCommentRequestDto reqDto) {
        return ResponseEntity.ok(commentService.deleteComment(reqDto.getId(),reqDto.getCostumeId(), reqDto.getUserId()));
    }

    @PostMapping(value = "/get-all-comments")
    public ResponseEntity<?> getAllCommentsAndUserInfo(@RequestBody GetCommentRequestDto reqDto) {
        return ResponseEntity.ok(commentService.getAllCommentsByCostumesId(reqDto.getCostumeId()));
    }


}
