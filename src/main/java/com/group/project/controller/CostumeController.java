package com.group.project.controller;

import com.group.project.dto.costumes.CostumeDto;
import com.group.project.dto.costumes.CostumesDetailDto;
import com.group.project.service.CostumeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class CostumeController {
    private final CostumeService costumeService;

    public CostumeController(CostumeService costumeService) {
        this.costumeService = costumeService;
    }

    @GetMapping("/get-costumes")
    public List<CostumeDto> getAll() {
        return costumeService.findAllCostumes();
    }

    @GetMapping("/get-costumes/{id}")
    public CostumeDto getCostumeById(@PathVariable Long id) {
        return costumeService.findCostumeByEthnicId(id);
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/edit-costumes")
    public String editCostume(@RequestBody CostumeDto editCostumeDto) {
        return costumeService.editCostumeInfoById(editCostumeDto);
    }
    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/edit-description")
    public String editDesciption(@RequestBody CostumesDetailDto editCostumeDetailDto) {
        return costumeService.editCostumesDetailById(editCostumeDetailDto);
    }
}
