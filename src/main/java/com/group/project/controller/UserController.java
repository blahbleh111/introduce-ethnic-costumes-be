package com.group.project.controller;

import com.group.project.dto.auth.ChangePassRequestDto;
import com.group.project.dto.auth.SignInRequestDto;
import com.group.project.dto.auth.SignUpRequestDto;
import com.group.project.exception.EmailAlreadyTakenException;
import com.group.project.exception.UsernameAlreadyTakenException;
import com.group.project.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }
    @PostMapping("/sign-up")
    public ResponseEntity<String> signup(@RequestBody SignUpRequestDto req) throws EmailAlreadyTakenException, UsernameAlreadyTakenException {
        return ResponseEntity.ok(userService.signup(req.getUsername(), req.getEmail(), req.getPassword()));
    }

    @PostMapping("/sign-in")
    public ResponseEntity<String> login(@RequestBody SignInRequestDto req) {
        return ResponseEntity.ok(userService.login(req));
    }

    @PreAuthorize("hasAnyAuthority('admin')")
    @PostMapping("/change-password")
    public ResponseEntity<?> changePass(@RequestBody ChangePassRequestDto req) {
        return ResponseEntity.ok(userService.changePass(req.getUserId(), req.getNewPass()));
    }
}
