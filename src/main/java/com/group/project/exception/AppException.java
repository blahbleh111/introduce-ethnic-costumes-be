package com.group.project.exception;

public class AppException extends Exception {
    public AppException(String message) {
        super(message);
    }
}
