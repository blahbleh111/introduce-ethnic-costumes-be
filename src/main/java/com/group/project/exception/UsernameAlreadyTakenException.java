package com.group.project.exception;

public class UsernameAlreadyTakenException extends Exception{
    public UsernameAlreadyTakenException(String message) {
        super(message);
    }
}
