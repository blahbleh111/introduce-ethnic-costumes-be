package com.group.project.exception;

public class EmailAlreadyTakenException extends Exception{
    public EmailAlreadyTakenException(String message) {
        super(message);
    }
}
