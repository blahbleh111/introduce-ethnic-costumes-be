package com.group.project.dto.inventory;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class RemoveInventoryCostumeRequestDto {
    @NotNull
    private Long costumeId;
    @NotNull
    private String size;
    @NotNull
    private Long quantity;
}
