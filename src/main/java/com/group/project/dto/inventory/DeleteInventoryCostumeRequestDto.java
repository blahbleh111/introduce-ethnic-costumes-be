package com.group.project.dto.inventory;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DeleteInventoryCostumeRequestDto {
    @NotNull
    private Long id;
}
