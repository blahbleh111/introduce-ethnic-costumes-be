package com.group.project.dto.inventory;

import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Data
public class InventoryCostumeDto {
    private Long id;
    private Long costumeId;
    private String size;
    private Long quantity;
}
