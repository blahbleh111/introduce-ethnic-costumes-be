package com.group.project.dto.ethnics;

import lombok.Data;

@Data
public class EthnicsDto {
    private Long id;
    private String name;
    private String description;
    private String imageUrl;
}
