package com.group.project.dto.ethnics;

import lombok.Data;

@Data
public class SearchEthnicsRequestDto {
    private String name;
}
