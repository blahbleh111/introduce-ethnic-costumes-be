package com.group.project.dto.order;

import lombok.Data;

@Data
public class OrderDetailDto {
    private Long costumeId;
    private Long quantity;
    private Float price;
    private String size;
    private String name;
    private String phoneNumber;
    private String address;
}
