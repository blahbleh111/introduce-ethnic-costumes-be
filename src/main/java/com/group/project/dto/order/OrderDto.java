package com.group.project.dto.order;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OrderDto {
    private Long id;
    private Long userId;
    private Float totalAmount;
    private LocalDateTime orderDate;
    private String status;
}
