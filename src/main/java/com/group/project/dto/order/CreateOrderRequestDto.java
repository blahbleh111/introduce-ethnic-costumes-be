package com.group.project.dto.order;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateOrderRequestDto {
    @NotNull
    private Long costumeId;
    @NotNull
    private Long quantity;
    @NotNull
    private Float price;
    @NotNull
    private String size;
    @NotNull
    private String name;
    @NotNull
    private String phoneNumber;
    @NotNull
    private String address;
}
