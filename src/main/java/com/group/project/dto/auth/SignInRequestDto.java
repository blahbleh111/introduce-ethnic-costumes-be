package com.group.project.dto.auth;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SignInRequestDto {
    private String username;

    private String password;
}
