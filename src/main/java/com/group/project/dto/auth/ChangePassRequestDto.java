package com.group.project.dto.auth;

import lombok.Data;

@Data
public class ChangePassRequestDto {
     private Long  userId;
     private String newPass;
}
