package com.group.project.dto.auth;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class SignUpRequestDto {
    @NotEmpty(message = "Username cannot be empty")
    private String username;

    @NotEmpty(message = "Email cannot be empty")
    private String email;

    @Min(value = 8, message ="Password must be at least 8")
    @NotEmpty(message = "Password cannot be empty")
    private String password;

}
