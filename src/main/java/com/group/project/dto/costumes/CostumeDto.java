package com.group.project.dto.costumes;

import com.group.project.model.CostumesDetail;
import lombok.Data;

import java.util.List;

@Data
public class CostumeDto {
    private Long id;

    private Long ethnicId;

    private String material;

    private String pattern;

    private String characteristic;

    private String imageUrl;

    private Float price;

    private String currency;

    private String other;

    private List<CostumesDetail> listCostumesDetail;

}
