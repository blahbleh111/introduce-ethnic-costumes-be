package com.group.project.dto.costumes;

import lombok.Data;

@Data
public class CostumesDetailDto {
    private Long sexId;
    private String description;
}
