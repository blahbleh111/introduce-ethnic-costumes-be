package com.group.project.dto.comment;

import lombok.Data;

@Data
public class GetCommentResponseDto {
    private CommentDto commentDto;
    private String name;
    private String url;
}
