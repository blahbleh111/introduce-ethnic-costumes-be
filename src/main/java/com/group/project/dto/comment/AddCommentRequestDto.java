package com.group.project.dto.comment;

import lombok.Data;

@Data
public class AddCommentRequestDto {
    String content;
    Long costumeId;
}
