package com.group.project.dto.comment;

import lombok.Data;

@Data
public class GetCommentRequestDto {
    private Long costumeId;
}
