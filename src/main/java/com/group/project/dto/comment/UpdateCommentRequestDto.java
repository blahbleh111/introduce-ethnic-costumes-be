package com.group.project.dto.comment;

import lombok.Data;

@Data
public class UpdateCommentRequestDto {
    private Long id;
    private String content;
    private Long costumeId;
}
