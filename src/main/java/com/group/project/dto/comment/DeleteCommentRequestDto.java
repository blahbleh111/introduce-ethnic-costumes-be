package com.group.project.dto.comment;

import lombok.Data;

@Data
public class DeleteCommentRequestDto {
    Long id;
    Long costumeId;
    Long userId;
}
