package com.group.project.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "costumes_detail")
@Getter
@Setter
public class CostumesDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "costume_id")
    private Long costumeId;

    @Column(name = "sex")
    private String sex;

    @Column(name = "description")
    private String description;
}
