package com.group.project.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.repository.EntityGraph;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "costumes")
@NamedEntityGraph(name = "costumes")
public class CostumeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "ethnics_id")
    private Long ethnicId;

    @Lob
    @Column(name = "material")
    private String material;

    @Lob
    @Column(name = "pattern")
    private String pattern;

    @Lob
    @Column(name = "characteristic")
    private String characteristic;

    @Lob
    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "price")
    private Float price;

    @Column(name = "currency", length = 10)
    private String currency;

    @Lob
    @Column(name = "other")
    private String other;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @OneToMany
    @JoinColumn(name = "costume_id")
    private List<CostumesDetailEntity> listCostumesDetail;

}